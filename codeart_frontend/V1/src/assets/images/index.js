//////APP///////
export { default as favicon } from './app/favicon.ico';
export { default as backgroundX } from './backgrounds/background-X.jpg';
export { default as backgroundY } from './backgrounds/background-Y.jpg';
export { default as backgroundZ } from './backgrounds/background-Z.jpg';
