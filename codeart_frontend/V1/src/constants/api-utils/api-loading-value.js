const bufferArray = ['Adding Hidden Agendas', 'Adjusting Bell Curves', 'Aesthesizing Industrial Areas', 'Aligning Covariance Matrices',
    'Applying Feng Shui Shaders', 'Asserting Packed Exemplars', 'Building Data Trees', 'Bureacritizing Bureaucracies', 'Calculating Inverse Probability Matrices',
    'Calculating Llama Expectoration Trajectory', 'Cohorting Exemplars', 'Compounding Inert Tessellations', 'Computing Optimal Bin Packing', 'Concatenating Sub-Contractors',
    'Debunching Unionized Commercial Services', 'Deciding What Message to Display Next', 'Decomposing Singular Values', 'Depositing Slush Funds', 'Destabilizing Economic Indicators',
    'Factoring Pay Scale', 'Fixing Election Outcome Matrix', 'Gathering Particle Sources', 'Generating Jobs', 'Gesticulating Mimes', 'Graphing Whale Migration',
    'Hiding Willio Webnet Mask', 'Implementing Impeachment Routine', 'Inserting Sublimated Messages', 'Integrating Curves', 'Normalizing Power', 'Obfuscating Quigley Matrix',
    'Overconstraining Dirty Industry Calculations', 'Perturbing Matrices', 'Realigning Alternate Time Frames', 'Reconfiguring User Mental Processes', 'Relaxing Splines',
    'Resolving GUID Conflict', 'Reticulating Splines', 'Retracting Phong Shader', 'Retrieving from Back Store', 'Reverse Engineering Image Consultant', 'Routing Neural Network Infanstructure',
    'Seeding Architecture Simulation Parameters', 'Sequencing Particles', 'Setting Advisor Moods', 'Setting Universal Physical Constants', 'Speculating Stock Market Indices',
    'Splatting Transforms', 'Synthesizing Gravity', 'Synthesizing Wavelets', 'Zeroing Crime Network']

export function bufferArrayResults() {
    const bufferArrayValue = bufferArray[Math.floor(Math.random() * bufferArray.length)];
    const returnValue = bufferArrayValue;
    return returnValue
}
