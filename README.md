# SUDO Node #

to run node as a sudo user:
`sudo "$(which node)" javascriptfile.js`

to run npm as a sudo user:
`sudo "$(which npm)" install -g angular-cli`

`to pull submodule `
git submodule update --init --recursive

`install typescript globally (as root)`
npm install -g typescript 

`scott note`
had to add this in the splitflapjs folder: npm i --save-dev @types/node
working with Node:

v12.22.5
